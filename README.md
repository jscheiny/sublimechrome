# Sublime Chrome

Adds sublime style palettes to chrome for navigating tabs and commands. There
are currently two panels: Tabs panel (Cmd+K) and Commands panel (Shift+Cmd+K).

## Installation

Go Window > Extensions. Make sure the developer mode box is checked. Select
'Load unpacked extension' and open this folder.
