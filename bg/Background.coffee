lastCommand = undefined
currWindowId = undefined

WIDTH = 600
HEIGHT = 500

createWindow = (command) ->
  lastCommand = command
  leftBound = (window.screen.availWidth / 2) - (WIDTH / 2)
  options =
    url     : "palette.html##{command}"
    focused : true
    width   : WIDTH
    height  : HEIGHT
    left    : leftBound
    type    : 'popup'
  chrome.windows.create options, (wind) ->
    currWindowId = wind.id

chrome.commands.onCommand.addListener (command) ->
  if currWindowId?
    if command == lastCommand
      chrome.windows.remove currWindowId, (->)
    else
      chrome.windows.remove currWindowId, -> createWindow(command)
  else
    createWindow(command)

chrome.windows.onRemoved.addListener (windowId) ->
  if currWindowId == windowId
    currWindowId = undefined
