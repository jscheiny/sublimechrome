subl = (window.subl ?= {})

class subl.QueryModel extends Backbone.Model
  defaults:
    query: ''
    results: undefined

  initialize: (options) ->
    @set 'results', options.results

  performQuery: (query) ->
    query = query.toLowerCase()
    if query isnt @get 'query'
      @set 'query', query
      @get('results').filter query
