subl = (window.subl ?= {})

class subl.ResultsView extends Backbone.Marionette.CollectionView
  tagName   : "div"
  className : "results-list"
  childView : Backbone.Marionette.ItemView
  template  : subl.templates['ResultsView']

  collectionEvents:
    'reset': 'render'
