subl = (window.subl ?= {})

class subl.CommandModel extends Backbone.Model
  defaults:
    title       : undefined
    final       : true
    action      : (->)
    highlighted : false

  initialize: ->
    @set 'titleLC', @get('title').toLowerCase()

  test: (query) ->
    title = subl.queries.perform @get('titleLC'), query
    @set
      ranges : title.ranges
      score  : title.score
    return title.found

  highlight: -> @set 'highlighted', true
  unhighlight: -> @set 'highlighted', false

  select: ->
    @get('action')(->
      window.close()
    )
