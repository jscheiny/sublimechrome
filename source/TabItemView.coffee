subl = (window.subl ?= {})

class subl.TabItemView extends Backbone.Marionette.ItemView
  template  : subl.templates['TabItemView']
  tagName   : 'div'
  className : 'tab result'

  modelEvents:
    'change:highlighted': 'highlight'

  events:
    'click': 'select'

  select: ->
    @model.select()

  highlight: (model, highlighted) ->
    @$el.toggleClass 'highlight', highlighted
