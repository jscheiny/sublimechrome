subl = (window.subl ?= {})

TITLE_BAR_HEIGHT = 22

class subl.ResultsCollection extends Backbone.Collection
  initialize: (models, options) ->
    @_sources = models.slice()
    @_filtered = false
    @_setSelection -1
    @on 'reset', @_resize
    @_resize()

  comparator: (model) ->
    if @_filtered then -model.get('score') else model.get('titleLC')

  filter: (query) ->
    if query.length isnt 0
      @_filtered = true
      filteredModels = []
      for model in @_sources
        if model.test query
          filteredModels.push model
      @reset filteredModels
      if filteredModels.length is 0
        @_setSelection -1
      else
        @_setSelection 0
    else
      @_filtered = false
      @reset @_sources
      @_setSelection -1

  selectHighlighted: ->
    if @_selected?
      @_selected.select()

  highlightNext: ->
    if @_selectedIndex == -1
      @_setSelection 0
    else if @_selectedIndex < @models.length - 1
      @_setSelection @_selectedIndex + 1

  highlightPrev: ->
    if @_selectedIndex == -1
      @_setSelection @models.length - 1
    else if @_selectedIndex > 0
      @_setSelection @_selectedIndex - 1

  _setSelection: (index) ->
    @_selected?.unhighlight()
    @_selectedIndex = index
    if @_selectedIndex == -1
      @_selected = undefined
    else
      @_selected = @models[@_selectedIndex]
      @_selected.highlight()

  _resize: ->
    chrome.windows.getCurrent (curr) ->
      options =
        height : $('body').outerHeight() + TITLE_BAR_HEIGHT
      chrome.windows.update curr.id, options, (->)

