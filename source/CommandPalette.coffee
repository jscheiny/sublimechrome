subl = (window.subl ?= {})

chromePage = (title, url) -> new subl.CommandModel
  title  : "Chrome: #{title}"
  final  : true
  action : (callback) ->
    chrome.windows.getLastFocused (w) ->
      options =
        windowId : w.id
        url      : "chrome://#{url}"
        active   : true
      chrome.tabs.create options, callback

about      = chromePage 'About', 'chrome'
apps       = chromePage 'Apps', 'apps'
bookmarks  = chromePage 'Bookmarks', 'bookmarks'
devices    = chromePage 'Devices', 'devices'
downloads  = chromePage 'Downloads', 'downloads'
extensions = chromePage 'Extensions', 'extensions'
history    = chromePage 'History', 'history'
memory     = chromePage 'Memory', 'memory'
plugins    = chromePage 'Plugins', 'plugins'
settings   = chromePage 'Settings', 'settings'

commands = [about, apps, bookmarks, devices, downloads, extensions, history, memory, plugins, settings]

subl.ResultsCreator ?= {}
subl.ResultsCreator['#commands'] = (callback) ->
  resultsCollection = new subl.ResultsCollection commands,
    model  : subl.CommandModel
  resultsView = new subl.ResultsView
    collection : resultsCollection
    childView  : subl.CommandItemView
  callback(resultsView)
