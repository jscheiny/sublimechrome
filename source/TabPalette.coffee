subl = (window.subl ?= {})

getTabs = (windows, currWindow) ->
  tabs = []
  for w in windows
    if w.id != currWindow.id
      for tab in w.tabs
        tabs.push tab
  return tabs

createTabModel = (tab) ->
  return new subl.TabModel
    tab   : tab
    title : tab.title
    url   : tab.url

subl.ResultsCreator ?= {}
subl.ResultsCreator['#tabs'] = (callback) ->
  chrome.windows.getCurrent (currWindow) ->
    chrome.windows.getAll {populate: true}, (windows) ->
      tabModels = []
      for tab in getTabs windows, currWindow
        tabModels.push createTabModel(tab)
      resultsCollection = new subl.ResultsCollection tabModels,
        model  : subl.TabModel
      resultsView = new subl.ResultsView
        collection : resultsCollection
        childView  : subl.TabItemView
      callback(resultsView)
