Handlebars.registerHelper 'tag', (source, ranges) ->
  if ranges? and ranges.length isnt 0
    lastIndex = 0
    result = ''
    for [startIndex, length] in ranges
      result += Handlebars.Utils.escapeExpression source.substring(lastIndex, startIndex)
      outside = Handlebars.Utils.escapeExpression source.substr(startIndex, length)
      result += "<span class=\"glow\">" + outside + "</span>"
      lastIndex = startIndex + length
    if lastIndex < source.length
      result += Handlebars.Utils.escapeExpression source.substr(lastIndex)
    return new Handlebars.SafeString(result)
  return source
