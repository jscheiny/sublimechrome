subl = (window.subl ?= {})

class subl.CommandItemView extends Backbone.Marionette.ItemView
  template  : subl.templates['CommandItemView']
  tagName   : 'div'
  className : 'command result'

  modelEvents:
    'change:highlighted': 'highlight'

  events:
    'click': 'select'

  select: ->
    @model.select()

  highlight: (model, highlighted) ->
    @$el.toggleClass 'highlight', highlighted
