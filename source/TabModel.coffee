subl = (window.subl ?= {})

TITLE_WEIGHT = 1.5
URL_WEIGHT = 1

class subl.TabModel extends Backbone.Model
  defaults:
    title       : undefined
    url         : undefined
    highlighted : false

  initialize: ->
    @set 'titleLC', @get('title').toLowerCase()
    @set 'urlLC', @get('url').toLowerCase()

  test: (query) ->
    title = @_queryField 'title', query
    url = @_queryField 'url', query
    @set
      titleRanges : title.ranges
      urlRanges   : url.ranges
      score       : (TITLE_WEIGHT * title.score) + (URL_WEIGHT * url.score)
    return title.found or url.found

  highlight: -> @set 'highlighted', true
  unhighlight: -> @set 'highlighted', false

  select: ->
    tab = @get 'tab'
    chrome.windows.update tab.windowId, {focused: true}, ->
      chrome.tabs.update tab.id, {active: true}, ->
        window.close()

  _queryField: (field, query) ->
    sourceLC = @get "#{field}LC"
    source = @get field
    return subl.queries.perform sourceLC, query
