subl = (window.subl ?= {})

class QueryView extends Backbone.Marionette.ItemView
  template: subl.templates['QueryView']

  ui:
    text: '#query'

  events:
    'change @ui.text' : '_performQuery'
    'paste @ui.text'  : '_performQuery'
    'keyup @ui.text'  : '_performQuery'

  onRender: ->
    @focus()

  initialize: (options) ->
    {@model} = options

  focus: -> @ui.text.focus()

  _performQuery: (evt)->
    if evt.keyCode in [13, 40, 48, 27]
      evt.preventDefault()
      return true
    @model.performQuery @ui.text.val()
    return true

subl.CreateQueryView = (results) ->
  return new QueryView
    model: new subl.QueryModel {results}
