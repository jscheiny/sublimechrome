subl = (window.subl ?= {})

queryHelper = (haystack, needle, hIndex, nIndex) ->
  if nIndex == needle.length
    return [[]]
  else if hIndex == haystack.length
    return []
  results = []
  for index in [hIndex...haystack.length]
    if haystack[index] == needle[nIndex]
      for subindices in queryHelper(haystack, needle, index + 1, nIndex + 1)
        indices = [index]
        indices.push subindices...
        results.push indices
  return results

makeRanges = (indices) ->
  prev = -2
  currStart = -1
  currLength = -1
  result = []
  for curr in indices
    if curr == prev + 1
      currLength++
    else
      if currStart != -1
        result.push([currStart, currLength])
      currStart = curr
      currLength = 1
    prev = curr
  if currStart != -1
    result.push([currStart, currLength])
  return result

scoreRanges = (ranges) ->
  score = 0
  for [start, length] in ranges
    score += length * length / Math.log(5 + start)
  return score

NotFound =
  found   : false
  score   : -1
  ranges  : undefined
  indices : undefined

subl.queries =
  perform: (haystack, needle) ->
    maxScore   = -Infinity
    maxIndices = undefined
    maxRanges  = undefined
    results = queryHelper haystack, needle, 0, 0
    return NotFound if results.length is 0
    for indices in results
      ranges = makeRanges indices
      score  = scoreRanges ranges
      if score > maxScore
        maxScore   = score
        maxRanges  = ranges
        maxIndices = indices
    return {
      found   : true
      score   : maxScore
      ranges  : maxRanges
      indices : maxIndices
    }
