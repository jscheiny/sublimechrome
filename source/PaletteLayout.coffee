subl = (window.subl ?= {})

class PaletteLayoutView extends Backbone.Marionette.LayoutView
  el: 'body'
  template: subl.templates['PaletteView']
  tagName: 'div'
  className: 'content'

  events:
    'keyup': '_onKeyUp'
    'click': '_refocus'

  initialize: ({@resultsView, @queryView}) ->
    @results = @resultsView.collection

  _onKeyUp: (evt) ->
    if evt.keyCode == 13 # enter
      @results.selectHighlighted()
    else if evt.keyCode == 40 # down
      @results.highlightNext()
    else if evt.keyCode == 38 # up
      @results.highlightPrev()
    else if evt.keyCode == 27 # escape
      window.close()
    evt.preventDefault()
    evt.stopPropagation()

  _refocus: ->
    @queryView.focus()

$ ->
  subl.ResultsCreator[window.location.hash] (resultsView) ->
    try
      queryView = subl.CreateQueryView resultsView.collection
      paletteView = new PaletteLayoutView {resultsView, queryView}
      paletteView.addRegions
        queryRegion   : '#query-container'
        resultsRegion : '#results-container'
      paletteView.render()
      paletteView.queryRegion.show queryView
      paletteView.resultsRegion.show resultsView
      queryView.focus()
    catch exception
      console.error exception.stack
